using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Eakkarat.Gamedev3
{
    public class SimpleHealthPointComponent : MonoBehaviour
    {
        [SerializeField] public const float MAX_HP = 100;

        [SerializeField] private float m_healthPoint;

        public float healthPoint
        {
            get
            {
                return m_healthPoint;
            }
            set
            {
                if (value > 0)
                {
                    if (value <= MAX_HP)
                    {
                        m_healthPoint = value;
                    }
                    else
                    {
                        m_healthPoint = MAX_HP;
                    }
                }
            }
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
