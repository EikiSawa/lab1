using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eakkarat.Gamedev3
{
    public class ItemTypeComponent : MonoBehaviour
    {
        [SerializeField] protected ItemType m_ItemTpye;

        public ItemType Type
        {
            get
            {
                return m_ItemTpye;
            }
            set
            {
                m_ItemTpye = value;
            }
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
